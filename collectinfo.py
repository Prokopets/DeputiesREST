import requests
from bs4 import BeautifulSoup
import re
from deputies.models import Deputy


html = requests.get('http://w1.c1.rada.gov.ua/pls/site2/fetch_mps?skl_id=9')
html = html.text

soup = BeautifulSoup(html, 'html.parser')

#with open('res.html', 'w') as f:
# 	print(html, file=f)
for deputy in soup.select('ul > li'):
	photo = deputy.p.img['src']
	name = deputy.find_all('p')[1].a.text
	shift=0
	deputy_id=None
	if len(deputy.dl.find_all('dd')) == 4:
		shift=-1
	selected = deputy.dl.find_all('dd')[0].text
	group = deputy.dl.find_all('dd')[1].text
	if shift==0:
		deputy_id = deputy.dl.find_all('dd')[2].text
	fraction = deputy.dl.find_all('dd')[3+shift].text
	position = deputy.dl.find_all('dd')[4+shift].text
	#print()


	#print(photo, name, selected, group, deputy_id, fraction, position)
	obj = Deputy(name = name,
				selected=selected,
				number_in_list=deputy_id,
				fraction=fraction,
				group=group,
				photo=photo,
		)
	obj.save()
	print(obj)
