# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Deputy

@admin.register(Deputy)
class DeputyAdmin(admin.ModelAdmin):
	list_fields=('name', 'number_in_list')

# Register your models here.
