# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

from rest_framework import generics
from .models import Deputy
from .serializers import DeputySerializer


class DeputiesView(generics.ListAPIView):
    queryset = Deputy.objects.all()
    serializer_class = DeputySerializer

class CertainDeputyView(generics.RetrieveDestroyAPIView):
	serializer_class = DeputySerializer
	queryset = Deputy.objects.all()
	lookup_field='name'