# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class Deputy(models.Model):
	selected = models.CharField(max_length=200)
	name = models.CharField(max_length=100)
	group = models.CharField(max_length=200)
	fraction = models.CharField(max_length=200)
	number_in_list = models.IntegerField(blank=True, null=True)
	photo = models.CharField(max_length=2000)

	class Meta:
		verbose_name_plural='Deputies'
	def __str__(self):
		return "Name = {}\n ID={}".format(self.name, self.number_in_list)
# Create your models here.
